const User = require('../models/users.model')
const jwt = require('jsonwebtoken')
const handleError = require('../utils/handleErrors')
class UsersControllers {
    constructor() {
        this.createToken = this.createToken.bind(this)
        this.login = this.login.bind(this)
    }
     createToken(id){
        return jwt.sign({ id }, process.env.JWT, {
            expiresIn: 3 * 24 * 60 * 60
        });
    };
    signUpUser(req,res){
        const {email,password} = req.body
        console.log(req.body)
        const user = new User({email,password})
        user.save().then((user) =>{
            console.log('meow')
            res.status(200).json({success:true,user})
        }).catch(error=>{
            console.log(error)
        });
    }
    login(req,res){
        const {email,password} = req.body
        User.auth(email,password).then((user) =>{
            if(user.isActive){
                const token = this.createToken(user._id);
                res.status(200).json({success:true,token,email:user.email})
            }else{
                res.status(403).json({success:false,message:"In Active User"})
            }
        }).catch(err=>{
            const error = handleError(err)
            res.status(500).json({success:false,error})
        });
    }

    createBooking(req,res){
            
    }

}

module.exports = new UsersControllers();
