var express = require('express');
var router = express.Router();
const userControllers = require('../controllers/users.controllers')

/* GET users listing. */
router.post('/register', userControllers.signUpUser);
router.post('/login', userControllers.login);

module.exports = router;
