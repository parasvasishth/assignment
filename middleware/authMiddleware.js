const jwt = require('jsonwebtoken');
const User = require('../models/users.model');

const requireAuth = (req, res, next) => {
    const token = req.header.jwt;
    // check json web token exists & is verified
    if (token) {
        jwt.verify(token, process.env.JWT, (err, decodedToken) => {
            if (err) {
                console.log(err.message);
                res.status(403).json({success:false,message:"Not Authorized"});
            } else {
                console.log(decodedToken);
                next();
            }
        });
    } else {
        res.redirect('/login');
    }
};


module.exports = { requireAuth, checkUser };
